<?php

namespace kikocrud;

use Illuminate\Database\Eloquent\Model;

class ItemList extends Model
{	
	protected $table = 'item_lists';

    public function voucher()
    {
    	return $this->belongsTo('kikocrud\Voucher', 'voucher_id');
    }

    public function sup()
    {
    	return $this->belongsTo('kikocrud\Supplier', 'supplier_id');
    }
}
