<?php
use kikocrud\Product;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('home.welcome');
// });

Route::get('/', function() {
	return view('welcome');
});

Route::get('products', 
['as' => 'products', 'uses' => 'ProductsController@index']
);
// LIST
//Route::resource('lists', 'ListsController');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::post('products/add', 'ProductsController@add');

// Suppliers
Route::get('suppliers', ['as' => 'suppliers', 'uses' => 'SuppliersController@index']);
Route::post('suppliers/add', 'SuppliersController@add');

// Vouchers
Route::get('vouchers', ['as' => 'vouchers', 'uses' => 'VouchersController@index']);
Route::get('vouchers/list/{voucher_id}', ['as' => 'vouchers.additem', 'uses' => 'VouchersController@listo']);
Route::post('vouchers/add', 'VouchersController@add');
Route::post('vouchers/list/vouchers/list/add', 'VouchersController@additem');
Route::post('vouchers/list/vouchers/list/remarkitem', 'VouchersController@remarkitem');
// get product codes
Route::get('getproductcodes', function() {

	//$items = Product::all();

	$data = array(	    "suggestions" => array(
        array("value" => "one", "data" => "ON"),
        array("value" => "two", "data" => "TW"),
        array("value" => "three", "data" => "TH"),
        array("value" => "four", "data" => "FO"),
    ));

	// foreach ($items as $item) 
	// {
	// 	$data[] = $item->product_code;
	// }

	return Response::json($data);
});
