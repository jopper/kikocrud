<?php

namespace kikocrud\Http\Controllers;

use Illuminate\Http\Request;

use kikocrud\Http\Requests;
use kikocrud\Http\Controllers\Controller;
use kikocrud\Supplier;
use kikocrud\Product;

class SuppliersController extends Controller
{
    public function index() 
    {	
    	$products = Product::all();
    	$suppliers = Supplier::all();
    	
    	$data = array (
    		'items' => $products,
    		'suppliers' => $suppliers
    	);

    	return view('suppliers.index')->with($data);
    } 

    public function add(Request $req) 
    {
    	
    	$s = new Supplier;
    	$s->name = $req->input('name');
    	$s->address = $req->input('address');
    	$s->save();

    	return redirect()->action('SuppliersController@index');
    }
}
