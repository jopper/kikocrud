<?php

namespace kikocrud\Http\Controllers;

use Illuminate\Http\Request;

use kikocrud\Http\Requests;
use kikocrud\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    function index()
    {
    	return view('home.welcome');
    }
}
