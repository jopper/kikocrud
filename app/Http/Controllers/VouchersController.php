<?php

namespace kikocrud\Http\Controllers;

use Illuminate\Http\Request;

use kikocrud\Http\Requests;
use kikocrud\Http\Controllers\Controller;
use kikocrud\Voucher;
use kikocrud\ItemList;
use kikocrud\Product;

class VouchersController extends Controller
{
    public function index() 
    {	
    	$vouchers = Voucher::all();

    	$data = array(
    		'vouchers' => $vouchers
    	);

    	return view('vouchers.index')->with($data);
    } 

    public function add(Request $req) 
    {
    	
    	$s = new Voucher;
    	$s->voucher_no = $req->input('code');
    	$s->save();

    	return redirect()->action('VouchersController@index');
    }

    public function additem(Request $req) {
    	$item = new ItemList;
    	$item->product_code = $req->input('code');
    	$item->voucher_id = $req->input('voucher_id');
        $item->visible = 1;
    	$getSupplier = Product::where('product_code', '=', $item->product_code)->get();
 
    	foreach ($getSupplier as $sup) {
    		$supplierId = $sup->supplier_id;
    	}

    	$item->supplier_id = $supplierId;
    	$item->description = $req->input('description');
    	$item->save();
    	return redirect()->action('VouchersController@listo', array('voucher_id' => $req->input('voucher_id')));
    }

    public function remarkitem(Request $req) {
        $item = ItemList::find($req->input('item_id'));
        $item->visible = 0;
        $item->remark = $req->input('remark');
        $item->save();
        return redirect()->action('VouchersController@listo', array('voucher_id' => $req->input('voucher_id')));
    }

    public function listo($voucher_id)
    {
    	$listItems = ItemList::where('voucher_id', '=', $voucher_id)->get();
    	$codes = Product::all();
    	$data = array (
    		'items' => $listItems,
    		'codes' => $codes,
    		'voucher_id' => $voucher_id
    	);

    	return view('vouchers.list')->with($data);
    }
}
