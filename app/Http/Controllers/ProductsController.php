<?php

namespace kikocrud\Http\Controllers;

use Illuminate\Http\Request;

use kikocrud\Http\Requests;
use kikocrud\Http\Controllers\Controller;
use kikocrud\Product;
use kikocrud\Supplier;

class ProductsController extends Controller
{
    public function index() 
    {	
    	$products = Product::all();
    	$suppliers = Supplier::all();
    	
    	$data = array (
    		'items' => $products,
    		'suppliers' => $suppliers
    	);

    	return view('products.index')->with($data);
    }

    public function add(Request $req) 
    {
    	
    	$item = new Product;
    	$item->product_code =  $req->input('code');
    	$item->name = $req->input('name');
    	$item->supplier_id = $req->input('supplier');
    	$item->description = $req->input('description');
    	$item->save();

    	return redirect()->action('ProductsController@index');
    }


}
