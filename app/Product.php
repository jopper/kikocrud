<?php

namespace kikocrud;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{	
	//protected $fillable = ['product_code', 'name'];
    private $rules = [
    	'product_code' => 'required',
    	'name' => 'required'
    ];

    public function validate() {
    	$v = \Validator::make($this->attributes, $this->rules);
    	if ($v->passes()) return true;
    	$this->errors = $v->messages();
    	return false;
    }

    public function sup()
    {
    	return $this->belongsTo('kikocrud\Supplier', 'supplier_id');
    }
}
