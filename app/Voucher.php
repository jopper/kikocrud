<?php

namespace kikocrud;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public function items()
    {
    	return $this->hasMany('kikocrud\ItemList');
    }
}
