			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="/">
							<span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						<li>
							<a href="/" class="active"> 
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->
						<!-- BEGIN ITEM -->
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-content-paste"></i></div>
								<span class="title">Items</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{ URL::route('products') }}"><span class="title">Products</span></a></li>
								<li><a href="{{ URL::route('vouchers') }}" ><span class="title">Vouchers</span></a></li>
								<li><a href="{{ URL::route('suppliers') }}" ><span class="title">Suppliers</span></a></li>
								<li><a href="#" ><span class="title">Archive(coming soon)</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						<!-- END ITEM -->

					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; 2016</span> <strong>KIKOCRUD</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->