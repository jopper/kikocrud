@extends('layouts.master')

@section('content')
<?php use kikocrud\Product; ?>
<section class="style-default-bright">
	<div class="section-header">
		<h2 class="text-primary">Products <button id="showForm" type="button" class="btn ink-reaction btn-primary-dark"><i class="fa fa-plus"></i> Add Item</button></h2>
	</div>
	<div class="section-body">
		<!-- BEGIN DATATABLE 1 -->
		<div class="row">
			<div class="col-md-8">
<!-- 				<article class="margin-bottom-xxl">
					<p class="lead">
						kapag gusto mo i-edit ito punta ka lang resources/views/products/index.blade.php
						yung data nya nanggagaling sa app/Http/Controllers/ProductsController.php check mo yung index function :)
					</p>
				</article> -->
				@if (isset($status))
				    <div class="alert alert-success">
				        {{ $status }}
				        <?php unset($status); ?>
				    </div>
				@endif
			</div><!--end .col -->
		</div><!--end .row -->
		<div id="itemTable" class="row">
<!-- 			<div class="col-md-12">
				<h4>Example 1: Show/hide columns</h4>
			</div> --><!--end .col -->
			<div class="col-lg-12">
				<div class="table-responsive">
					<table id="datatable1" class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Product Code</th>
								<th>Name</th>
								<th class="sort-alpha">Supplier</th>
								<th>Desription</th>
								<th>Datetime</th>
							</tr>
						</thead>
						<tbody>
							@if ($items->count() >0)
								@foreach ($items as $item)
								<tr>
									<td>{{ $item->product_code }}</td>
									<td>{{ $item->name }}</td>
									<td>{{ $item->sup->name }}</td>
									<td>{{ $item->description }}</td>
									<td>{{ $item->created_at }}</td>
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div><!--end .table-responsive -->
			</div><!--end .col -->
		</div><!--end .row -->
		<!-- END DATATABLE 1 -->

<!-- BEGIN AUTOCOMPLETE -->
		<div id="addForm" class="row">

			<!--end .col -->
<!-- 			<div class="col-lg-3 col-md-4">
				<article class="margin-bottom-xxl">
					<p>
						Autocomplete inputs give you search options while typing.
						You can use either the autocomplete from Typeahead or jQuery, whichever you prefer.
					</p>
				</article>
			</div><!--end .col -->
			<div class="col-lg-offset-2 col-md-8">
				<div class="card style-default-dark">
					<div class="card-body form-inverse">
						<form class="form" action="products/add" method="post">
							<div class="form-group floating-label">
								<input type="text" id="autocomplete1" class="form-control" name="code" >
								<label>Product Code</label>
							</div>
							<div class="form-group floating-label">
								<input type="text" class="form-control" name="name" id="regular1">
								<label for="regular1">Name</label>
							</div>
							<div class="form-group floating-label">
								<select class="form-control select2-list" name="supplier" data-placeholder="Select supplier (optional)">
									<optgroup label="Suppliers">
									@if ($suppliers->count() > 0)
										@foreach ($suppliers as $supplier)
											<option value="{{$supplier->id}}">{{$supplier->name}}</option>
										@endforeach
									@endif
									</optgroup>
								</select>
								<label>Select Supplier</label>
							</div>
							<div class="form-group floating-label">
								<textarea  id="textarea1" name="description" class="form-control" rows="3" placeholder=""></textarea>
								<label for="textarea1">Description</label>
							</div>
							<button id="hideForm" type="button" class="btn ink-reaction btn-primary">Cancel</button>
							<button type="submit" class="btn ink-reaction btn-primary">Post</button>
						</form>
					</div><!--end .card-body -->
				</div><!--end .card -->
				<em class="text-caption">note: form validation is not applied yet - jop</em>
			</div><!--end .col -->
		</div><!--end .row -->
		<!-- END AUTOCOMPLETE -->
	<script type="text/javascript">
		$('#addForm').hide();
		$(document).ready(function() {
	    	$('#datatable1').DataTable();
		} );
	$("#showForm").click(function(){
	    $('#addForm').show(1000);
	    $("#itemTable").hide(1000);
	    $("#showForm").hide(1000);
	});

	$("#hideForm").click(function(){
	    $('#addForm').hide(1000);
	    $("#itemTable").show(1000);
	    $("#showForm").show(1000);
	});
	</script>
	</div><!--end .section-body -->
</section>

@endsection
