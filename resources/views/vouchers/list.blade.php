@extends('layouts.master')

@section('content')
<section class="style-default-bright">
	<div class="section-header">
		<h2 class="text-primary">Voucher <button id="showForm" type="button" class="btn ink-reaction btn-primary-dark">Add Product(Item)</button><button  type="button" class="btn ink-reaction btn-primary"> Back to Index</button></h2>
</button>
	</div>
	<div class="section-body">
		<!-- BEGIN DATATABLE 1 -->
		<div class="row">
			<div class="col-md-8">
<!-- 				<article class="margin-bottom-xxl">
					<p class="lead">
						kapag gusto mo i-edit ito punta ka lang resources/views/products/index.blade.php
						yung data nya nanggagaling sa app/Http/Controllers/ProductsController.php check mo yung index function :)
					</p>
				</article> -->
				@if (isset($status))
				    <div class="alert alert-success">
				        {{ $status }}
				        <?php unset($status); ?>
				    </div>
				@endif
			</div><!--end .col -->
		</div><!--end .row -->
		<div id="itemTable" class="row">
<!-- 			<div class="col-md-12">
				<h4>Example 1: Show/hide columns</h4>
			</div> --><!--end .col -->
			<div class="col-lg-12">
				<div class="table-responsive">
					<table id="datatable1" class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="sort-alpha">Product Code</th>
								<th>Voucher #</th>
								<th class="sort-alpha">Supplier</th>
								<th>Description</th>
								<th>Datetime</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@if ($items->count() >0)
								@foreach ($items as $item)
								<tr>
									@if ($item->visible == 0) 
									<td style="text-decoration: line-through;">{{ $item->product_code }}</td>
									<td style="text-decoration: line-through;">{{ $item->voucher->voucher_no }}</td>
									<td style="text-decoration: line-through;">{{ $item->sup->name }}</td>
									<td style="text-decoration: line-through;">{{ $item->description }}</td>
									<td style="text-decoration: line-through;">{{ $item->created_at }}</td>
									<td><button onclick="remark({{ $item->id }})" type="button" class="btn ink-reaction btn-sm btn-floating-action btn-danger"><i class="fa fa-minus"></i></button></td>
									@else
									<td>{{ $item->product_code }}</td>
									<td>{{ $item->voucher->voucher_no }}</td>
									<td>{{ $item->sup->name }}</td>
									<td>{{ $item->description }}</td>
									<td>{{ $item->created_at }}</td>
									<td><button onclick="remark({{ $item->id }})" type="button" class="btn ink-reaction btn-sm btn-floating-action btn-danger"><i class="fa fa-minus"></i></button></td>
									@endif
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div><!--end .table-responsive -->

					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info"></i> Your Remark</h4>
					      </div>
					      <div class="modal-body">
					        <form class="form" action="vouchers/list/remarkitem" method="post">
								<input type="hidden" id="itemId" class="form-control" name="item_id">
								<div class="form-group">
									<textarea  id="textarea1" name="remark" class="form-control" rows="3" placeholder=""></textarea>
									<label for="textarea1">Remark</label>
								</div>
								<input type="hidden" class="form-control" name="voucher_id" id="regular" value="{{ $voucher_id }}">
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-primary">Okay</button>
					        
					      </div>
					      </form>
					    </div>
					  </div>
					</div>
			</div><!--end .col -->


		</div><!--end .row -->
		<!-- END DATATABLE 1 -->
<!-- BEGIN AUTOCOMPLETE -->
		<div id="addForm" class="row">
<!-- 			<div class="col-lg-12">
				<h4>Add Product(Item)</h4>
			</div> -->
			<!--end .col -->
<!-- 			<div class="col-lg-3 col-md-4">
				<article class="margin-bottom-xxl">
					<p>
						Autocomplete inputs give you search options while typing.
						You can use either the autocomplete from Typeahead or jQuery, whichever you prefer.
					</p>
				</article>
			</div><!--end .col -->
			<div class="col-lg-offset-2 col-md-8">
				<div class="card style-default-dark">
					<div class="card-body form-inverse">
						<form class="form" action="vouchers/list/add" method="post">
							<div class="form-group">
								<select class="form-control select2-list" name="code" data-placeholder="Select supplier (optional)">
									<optgroup label="Product Codes">
										<option value=""></option>
									@if ($codes->count() > 0)
										@foreach ($codes as $c)
											<option value="{{$c->product_code}}">{{$c->product_code}}</option>
										@endforeach
									@endif
									</optgroup>
								</select>
								<label>Select Product Code</label>
							</div>
							<div class="form-group">
								<textarea  id="textarea1" name="description" class="form-control" rows="3" placeholder=""></textarea>
								<label for="textarea1">Description</label>
								<input type="hidden" class="form-control" name="voucher_id" id="regular" value="{{ $voucher_id }}">
							</div>
							<button id="hideForm" type="button" class="btn ink-reaction btn-primary">Cancel</button>
							<button type="submit" class="btn ink-reaction btn-primary">Post</button>
						</form>
					</div><!--end .card-body -->
				</div><!--end .card -->
				<em class="text-caption">note: form validation is not applied yet - jop</em>
			</div><!--end .col -->
		</div><!--end .row -->
		<!-- END AUTOCOMPLETE -->

<script type="text/javascript">
function remark(item_id) {
	$('#itemId').val(item_id);
	$('#myModal').modal('show');
}
    $('#addForm').hide();
	$(document).ready(function() {
    	$('#datatable1').DataTable();
	} );

	$("#showForm").click(function(){
	    $('#addForm').show(1000);
	    $("#itemTable").hide(1000);
	    $("#showForm").hide(1000);
	});

	$("#hideForm").click(function(){
	    $('#addForm').hide(1000);
	    $("#itemTable").show(1000);
	    $("#showForm").show(1000);
	});

$(function () {
    'use strict';

    $('#autocomplete2').autocomplete({
    serviceUrl: 'getproductcodes',
    dataType: 'json',
    type: 'GET',
    onSelect: function (suggestion) {
        alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});

});

</script>
	</div><!--end .section-body -->
</section>

@endsection
